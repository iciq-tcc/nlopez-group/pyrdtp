"""Module to parse and transform the different output files generated by VASP.
"""
from pyRDTP import geomio
from io import IOBase
import numpy as np
import xml.etree.ElementTree


class Frequencies:
    """Frequencies readed from OUTCAR file

    Attributes:
        freqs (lst of obj:`Freq`): List containing the different obj:`Freq`.
        n_freqs(int): Number of frequencies.
        self.real(lst of obj:`Freq`): List containing only the real obj:`Freq`.
        self.imaginary(lst of obj:`Freq`):List containing only the imaginary
            frequencies.
        self.zpe(float): Calculated zero point energy for the given
            frequencies.
    """

    def __init__(self):
        self.freqs = []
        self.n_freqs = 0
        self._real = []
        self._ima = []
        self._zpe = 0
        self._update = False

    def __getitem__(self, other):
        if isinstance(other, str):
            if other in ['r', 'real']:
                lst = self.real
            elif other in ['i', 'imaginary']:
                lst = self.imaginary
            else:
                raise TypeError
        else:
            lst = self.freqs[other]
        return lst

    def __str__(self):
        string = ''
        if self.freqs is None:
            return string
        for freq in self.freqs:
            string += '{}\n'.format(str(freq))
        string = string[:-1]
        return string

    def __repr__(self):
        string = 'Freqs({}({}i))'.format(len(self.freqs), len(self.imaginary))
        return string

    @classmethod
    def from_outcar(cls, obj):
        """Read the frecuencies from an OUTCAR file.

        Args:
            obj(str or file): Path or file containg the OUTCAR file.

        Returns:
            obj:`Frequencies` containing the readed frequencies.

        Notes:
            NWRITE = 2 and IBRION = 5, 6 or 8 are required to read the
            frequencies from the OUTCAR file.
        """
        frequencies = cls()
        if isinstance(obj, IOBase):
            outcar = obj.readlines()
        elif isinstance(obj, str):
            with open(obj, 'r') as outcar:
                outcar = outcar.readlines()
        else:
            raise NotImplementedError
        lines = [line.strip() for line in outcar]
        token = 'Eigenvectors and eigenvalues'
        for index, text in enumerate(lines):
            if token in text:
                begin_line = index
                break

        checker = False
        for index, line in enumerate(outcar[begin_line:]):
            text = line.split()
            if not checker:
                if len(text) < 2:
                    continue
                if text[1] == 'f' or text[1] == 'f/i=':
                    is_ima = 'f/i' in text[1]

                    if not is_ima:
                        text = text[1:]  # If not imaginary correct index

                    new_freq = Freq(thz=float(text[2]), pithz=float(text[4]),
                                    cm=float(text[6]), mev=float(text[8]),
                                    ima=is_ima)
                    frequencies.frequency_add(new_freq)
                    matrix = []
                    checker = True
                elif '=' in text:
                    break
            else:
                if not text:
                    new_freq.matrix = np.asarray(matrix)
                    checker = False
                elif text[0] == 'X':
                    continue
                else:
                    matrix.append(np.asarray(text[3:6], dtype=float))
        return frequencies

    def frequency_add(self, freq):
        """Add a frequency.

        Args:
            freq (obj:`Freq`): Frequency that will be added to the object.
        """
        self.freqs.append(freq)
        self.n_freqs += 1
        self._update = True
        return self

    def frequency_remove(self, freq):
        """Remove a frequency.
        Args:
            freq (obj:`Freq`): Frequency that will be removed from the object.
        """
        self.freqs.remove(freq)
        self.n_freqs -= 1
        self._update = True
        return self

    @property
    def real(self):
        """Filter and return the real frequencies.

        Returns:
            lst of obj:`Freq` containing only the real frequencies.
        """
        if self._update or not self._real:
            self._real = [freq for freq in self.freqs if not freq.ima]
        return self._real

    @property
    def imaginary(self):
        """Filter and return the imaginary frequencies.

        Returns:
            lst of obj:`Freq` containing only the imaginary frequencies.
        """
        if self._update or not self._ima:
            self._ima = [freq for freq in self.freqs if freq.ima]
        return self._ima

    @property
    def zpe(self):
        """Compute and return the zero point energy in eV.

        Returns:
            float, zero point energy in eV.
        """
        if self._update or not self._zpe:
            self._zpe = sum([freq.mev for freq in self.real]) / 2000
        return self._zpe


class Freq:
    """Calculated frequency.

    Attributes:
        thz (float): Energy in THz.
        pithz (float) = Energy in 2piTHz.
        cm (float): Energy in cm-1.
        mev (float): Energy in meV.
        ima (bool): True if the frequency is imaginary.
        matrix (3xn obj:`np.ndarray`): Array containing the dx, dy and dz
            values of the frequency.
        energy (lst of floats): lst containing the associated energies in
            different units.
    """
    def __init__(self, thz=None, pithz=None, cm=None, mev=None, ima=None,
                 matrix=None):
        self.thz = thz
        self.pithz = pithz
        self.cm = cm
        self.mev = mev
        self.ima = ima
        self.matrix = matrix

    def __str__(self):
        string = '{} THz, {} 2piTHz, {} cm-1, {}, meV'.format(*self.energy)
        if self.ima:
            string += ' i'
        return string

    def __repr__(self):
        if self.ima:
            ima = 'i'
        else:
            ima = ''
        string = 'freq({}{}cm-1)'.format(ima, self.cm)
        return string

    def __eq__(self, other):
        thresh = 0.001
        if isinstance(other, Freq):
            mat_bool = np.all(self.matrix, other.matrix)
            enr_bool = (self.cm - other.cm) < thresh
        return all((mat_bool, enr_bool))

    @property
    def energy(self):
        """Returns:
            List containing all the energies of the frequency
        """
        return [self.thz, self.pithz, self.cm, self.mev]


class Image:
    def __init__(self):
        self.energies = None
        self.forces = None
        self._molecule = None
        self._coords = None

    @property
    def molecule(self):
        if self._molecule is None:
            if self._coords is None:
                raise AttributeError('Coords needed to generate a molecule.')
            mol = geomio.MolObj()
            mol.universal_read(self._coords)
            self._molecule = mol.write()
        return self._molecule

    @molecule.setter
    def molecule(self, other):
        self._molecule = other

    @property
    def coords(self):
        if self._coords is None:
            if self._molecule is None:
                raise AttributeError('Molecule needed to generate coords.')
                pass
            coords = geomio.MolObj()
            coords.read(self._molecule)
            self._coords = coords.universal_convert()
        return self._coords

    @coords.setter
    def coords(self, other):
        if isinstance(other, geomio.AbsCoord):
            self._coords = other.universal_convert()
        elif isinstance(other, geomio.UniversalCoord):
            self._coords = other
        else:
            raise NotImplementedError


class Package:
    """Package containg different images.

    Attribues:
        images (lst of obj:`Image`): List containg the different images.
        n_images (int): Number of images in the package.
    """
    def __init__(self):
        self.images = []
        self.n_images = 0

    def __getitem__(self, other):
        return self.images[other]

    @classmethod
    def from_vasprun(cls, filename='vasprun.xml'):
        """Read all the images from a vasprun.xml file and return a list
        containing all the image objects.

        Args:
            filename(str, optional): Path of the vasprun.xml file. Defaults to
                vasprun.xml.
        Returns:
            obj:`Package` containing all the readed obj:`Image` from the run.
        """
        vasprun = xml.etree.ElementTree.parse(filename).getroot()
        calculations = vasprun.findall('calculation')
        atominfo = vasprun.find('atominfo')
        atominfo = atominfo.find('array')
        atominfo = atominfo.find('set')
        elem_lst = []
        for element in atominfo:
            elem_lst.append(element[0].text.strip())
        elem_lst = np.asarray(elem_lst)

        freeze = [match for match in vasprun.findall('structure')
                  if 'initialpos' in match.attrib.values()]
        freeze = [match for match in freeze[0].findall('varray')
                  if 'selective' in match.attrib.values()]
        freeze = freeze[0].findall('v')
        freeze = [line.text.split() for line in freeze]

        pack = cls()
        for item in calculations:
            forces = item.find('varray')
            forces = [line.text.split() for line in forces]
            forces = np.asarray(forces, dtype=float)

            dcoords = item.find('structure')
            cell_p_pvt = dcoords.find('crystal')
            dcoords = dcoords.find('varray')
            dcoords = [line.text.split() for line in dcoords]
            dcoords = np.asarray(dcoords, dtype=float)

            energy_pvt = item.find('energy').findall('i')
            energy = {}
            for item_pvt in energy_pvt:
                if 'e_fr_energy' in item_pvt.attrib.values():
                    energy['fr_energy'] = float(item_pvt.text)
                if 'e_wo_entrp' in item_pvt.attrib.values():
                    energy['wo_entrp'] = float(item_pvt.text)
                if 'e_0_energy' in item_pvt.attrib.values():
                    energy['0_energy'] = float(item_pvt.text)

            for item_pvt in cell_p_pvt:
                if 'basis' in item_pvt.attrib.values():
                    cell_p = [line.text.split() for line in item_pvt]
                    cell_p = np.asarray(cell_p, dtype=float)
                    break

            coords = geomio.UniversalCoord()
            coords.elements = elem_lst
            coords.additional_inf['cell_p'] = cell_p
            coords.additional_inf['freeze'] = freeze
            coords.additional_inf['dcoords'] = dcoords
            coords.coords = np.dot(dcoords, cell_p)

            img_tmp = Image()
            img_tmp.energy = energy
            img_tmp.forces = forces
            img_tmp.coords = coords

            pack.image_add(img_tmp)
        return pack

    def copy(self):
        """Copy the package and return it.

        Returns:
            obj:`Package` containing the same values as the original.
        """
        new_pack = Package()
        new_pack.images = self.images
        new_pack.n_images = self.n_images
        return new_pack

    def image_add(self, image):
        """Add an image to the package.

        Args:
            image (obj:`Image`): Image that will be added to the package.
        """
        self.images.append(image)
        self.n_images += 1

    def image_rm(self, image):
        """Remove an image from the package.

        Args:
            image(int or obj:`Image`): If a int is given, remove the Image
                associated with this index. If a obj:`Image` is given,
                remove this image if from the package.
        """
        if isinstance(image, int):
            del self.images[image]
        elif isinstance(image, Image):
            self.images.remove(image)
        else:
            raise NotImplementedError
        self.n_images = len(self.images)

    def package_add(self, pack):
        """Add the images from another package to this package.

        Args:
            pack (obj:`Package`): Package that will be added to the original
                one.
        """
        self.images += pack.images
        self.n_images += pack.n_images

    def molecules_get(self):
        """Get all the molecules associated with the images in the package.

        Returns:
            list of obj:`pyRDTP.molecule.Molecule` containing the molecules
                associated with the images.
        """
        return [img.molecule for img in self.images]

    def energies_get(self):
        """Get all the energies associated with the images in the package.

        Returns:
            list of dict containing the different energies of the images.
        """
        return [img.energy for img in self.images]
